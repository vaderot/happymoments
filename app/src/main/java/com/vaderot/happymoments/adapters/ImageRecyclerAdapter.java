package com.vaderot.happymoments.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vaderot.happymoments.R;
import com.vaderot.happymoments.db.ImagesTable;
import com.vaderot.happymoments.maodels.Image;
import com.vaderot.happymoments.widgets.LoaderImageView;

/**
 * Created by Admin on 13.08.2015.
 */
public class ImageRecyclerAdapter extends BaseCursorRecyclerViewAdapter<ImageRecyclerAdapter.ImageViewHolder>{

    public ImageRecyclerAdapter(Context context) {
        super(context);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        if(cursor != null && cursor.moveToPosition(position)){
            Image image = ImagesTable.cursorToObject(cursor);
            setupImageViewHolser(holder, image);
        }
    }

    private void setupImageViewHolser(ImageViewHolder holder, Image image){
        Bitmap bitmap = null;
        int imageId = Integer.valueOf(image.getImageUri().getLastPathSegment());
        Cursor cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(
                context.getContentResolver(), Integer.valueOf(imageId),
                MediaStore.Images.Thumbnails.MINI_KIND,
                null);
        if( cursor != null && cursor.moveToFirst() ) {
            String thumbImagePath = cursor.getString( cursor.getColumnIndex( MediaStore.Images.Thumbnails.DATA ));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(thumbImagePath, options);
            holder.ivIimage.setImageBitmap(bitmap);

        } else {
             cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(
                    context.getContentResolver(), Integer.valueOf(imageId),
                    MediaStore.Images.Thumbnails.MICRO_KIND,
                    null);
            if( cursor != null && cursor.moveToFirst() ) {
                String thumbImagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bitmap = BitmapFactory.decodeFile(thumbImagePath, options);
                holder.ivIimage.setImageBitmap(bitmap);
            }else {
                holder.ivIimage.load(image.getImageUri().toString());
            }
        }
        if(cursor != null && !cursor.isClosed())
            cursor.close();


        holder.ivIimage.getRootView().setTag(image.getId());


        holder.tvImageNage.setText(image.getImageName());
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        private LoaderImageView ivIimage;
        private TextView tvImageNage;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ivIimage = (LoaderImageView) itemView.findViewById(R.id.image);
            tvImageNage = (TextView) itemView.findViewById(R.id.imageName);
        }
    }
}
