package com.vaderot.happymoments;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.vaderot.happymoments.db.DbHelper;

/**
 * Created by Admin on 13.08.2015.
 */
public class HappyMomentsApp extends Application {

    private static HappyMomentsApp sContext;

    public HappyMomentsApp() {
        super();
        sContext = this;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        new DbHelper(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(1)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .diskCacheFileCount(50)
                .memoryCacheSize(5 * 1024 * 1024)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheExtraOptions(480, 320, null)
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static HappyMomentsApp getApplication() {
        return sContext;
    }
}
