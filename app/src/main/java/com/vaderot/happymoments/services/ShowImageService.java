package com.vaderot.happymoments.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.SystemClock;
import android.widget.Toast;

import com.vaderot.happymoments.threads.DbRequesTask;
import com.vaderot.happymoments.threads.ShowImageTask;

/**
 * Created by Admin on 13.08.2015.
 */
public class ShowImageService extends IntentService {

    public static final String TAG = "ShowImageServiceTag";

    private ShowImageTask mShowImageTask;

    public ShowImageService() {
        super(ShowImageService.class.getName());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        new DbRequesTask(DbRequesTask.RequestType.GET_ALL_IMAGES){
            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                mShowImageTask = new ShowImageTask(ShowImageService.this, cursor);
                mShowImageTask.execute();
            }
        }.execute();

        return START_STICKY;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String s = intent.getAction();
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    //    https://groups.google.com/forum/#!topic/android-developers/H-DSQ4-tiac
    @Override
    public void onTaskRemoved(Intent rootIntent){
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
            restartServiceIntent.setPackage(getPackageName());

            PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmService.set(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + 1000,
                    restartServicePendingIntent);

            super.onTaskRemoved(rootIntent);
    }


    @Override
    public void onDestroy() {
        mShowImageTask.setStopped(true);
        super.onDestroy();
    }
}
