package com.vaderot.happymoments.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.vaderot.happymoments.Consts;
import com.vaderot.happymoments.R;
import com.vaderot.happymoments.fragments.AddImageFragment;
import com.vaderot.happymoments.fragments.EditImageFragment;
import com.vaderot.happymoments.fragments.MainFragment;
import com.vaderot.happymoments.fragments.SettingsFragment;
import com.vaderot.happymoments.services.ShowImageService;

import java.util.List;
/**
 * Created by Admin on 12.08.2015.
 */
public class BaseFragmentActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar mToolbar;

    protected Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        stopShowImageService(ShowImageService.TAG);

        setupToolBar();
        startFragment(new MainFragment(), false);
    }

    @Override
    protected void onDestroy() {
        startShowImageService(ShowImageService.TAG);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addImage:{
                ((AddImageFragment)currentFragment).addImage();
            }
            break;
            case R.id.editImage:{
                ((EditImageFragment)currentFragment).updateImage();
            }
            break;
            case R.id.deleteImage:{
                ((EditImageFragment)currentFragment).deleteImage();
            }
            break;
            case R.id.settings:{
                startFragment(new SettingsFragment(), true);
            }
            break;
            case R.id.itemImageView:{
                Bundle bundle = new Bundle();
                bundle.putInt(Consts.IMAGE_ID, (Integer) view.getTag());
                EditImageFragment editImageFragment = new EditImageFragment();
                editImageFragment.setArguments(bundle);
                startFragment(editImageFragment, true);
            }
            break;
            case R.id.fabAddImage:{
                startFragment(new AddImageFragment(), true);
            }
            break;


        }
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.action_settings:{
//                startFragment(new SettingsFragment(), true);
//            }
//            break;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void startShowImageService(final String tag) {
        Intent intent = new Intent(getApplicationContext(), ShowImageService.class);
        intent.addCategory(tag);
        startService(intent);
    }


    private void stopShowImageService(final String tag) {
        Intent intent = new Intent(getApplicationContext(), ShowImageService.class);
        intent.addCategory(tag);
        stopService(intent);
    }


    public Fragment startFragment(Fragment f, boolean addToBackStack) {
        return startFragment(f, addToBackStack, false);
    }

    public Fragment startFragment(Fragment f, boolean addToBackStack, boolean popBackStack) {
        currentFragment = startFragment(R.id.container, f, addToBackStack, popBackStack);
        return currentFragment;
    }

    protected Fragment startFragment(int viewId, Fragment f, boolean addToBackStack, boolean popBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        String tag = f.getClass().getSimpleName();

        if (popBackStack) {
            if (manager.findFragmentByTag(tag) != null) {
                List<Fragment> fragments = manager.getFragments();
                int index = -1;
                for (int i = 0; i < fragments.size(); i++) {
                    Fragment fr = fragments.get(i);
                    if (fr.getClass().getSimpleName().equals(tag)) {
                        index = i;
                        break;
                    }
                }

                if (index != -1) {
                    int popCount = fragments.size() - index - 1;
                    for (int i = 0; i < popCount; i++) {
                        manager.popBackStackImmediate();
                    }
                    return fragments.get(index);
                }
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        ft.replace(viewId, f, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }

        ft.commit();
        manager.executePendingTransactions();
        return f;
    }

    private void setupToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(mToolbar);
    }
    public Toolbar getToolbar() {
        return mToolbar;
    }
}
