package com.vaderot.happymoments.activities;

import android.app.Activity;
import android.os.Bundle;

import com.vaderot.happymoments.Consts;
import com.vaderot.happymoments.R;
import com.vaderot.happymoments.widgets.LoaderImageView;

/**
 * Created by Admin on 12.08.2015.
 */
public class ShowImageActivity extends Activity {

    private int displayDyration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        displayDyration = getIntent().getIntExtra(Consts.DISPLAY_DURATION, 500);
        String imageUri = getIntent().getStringExtra(Consts.IMAGE_URI);
        LoaderImageView imageView = (LoaderImageView)findViewById(R.id.image);
        imageView.load(imageUri);

    }


    @Override
    protected void onResume() {
        super.onResume();
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(displayDyration);
                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }
                ShowImageActivity.this.finish();
            }
        }).start();
    }
}
