package com.vaderot.happymoments;

/**
 * Created by Admin on 13.08.2015.
 */
public class Consts {
    public static final String IMAGE_URI = "imageUri";
    public static final String IMAGE_ID = "imageId";
    public static final String DISPLAY_DURATION = "displayDuration";
}
