package com.vaderot.happymoments.db;

import android.database.Cursor;
import android.text.TextUtils;

import com.vaderot.happymoments.maodels.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 12.08.2015.
 */
public class ImagesTable {
    public static final String NAME = "images";

    public enum Columns {
        ID(DbHelper.INT, "_id"),
        IMAGE_URI(DbHelper.TEXT, "image_uri"),
        IMAGE_NAME(DbHelper.TEXT, "image_name"),
        IMAGE_DESCRIPTION(DbHelper.TEXT, "image_description");

        String type;
        String name;

        private Columns(String type, String name) {
            this.type = type;
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static Image cursorToObject(Cursor cur) {
        Image image = new Image();

        image.setId(cur.getInt(0));
        image.setImageUri(cur.getString(1));
        image.setImageName(cur.getString(2));
        image.setImageDeskription(cur.getString(3));

        return image;
    }

    public static String getCreation(){
        String query = "CREATE TABLE IF NOT EXISTS " + NAME + " (_id " + DbHelper.INT + " PRIMARY KEY  AUTOINCREMENT, ";

        List<String> pairs = new ArrayList<String>();
        for (int i = 1; i < Columns.values().length; i++){
            pairs.add(Columns.values()[i] + " " + Columns.values()[i].getType());
        }
        query += TextUtils.join(", ", pairs);
        query += ");";

        return query;
    }

    public static Columns[] getColumns() {
        return Columns.values();
    }

    public static String[] getColumnsList() {
        Columns[] columns = getColumns();

        String[] values = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            values[i] = columns[i].getName();
        }

        return values;
    }
}