package com.vaderot.happymoments.db;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.vaderot.happymoments.HappyMomentsApp;

/**
 * Created by Admin on 14.08.2015.
 */
public class PrefHelper {
    private final static String PERIOD_UPDATE = "period_update";
    private final static String DISPLAY_DURATION = "display_duration ";
    private final static String RANDOM_DISPLAY = "random_display";

    public static void setPeriodUpdate(int periodUpdate) {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        SharedPreferences.Editor ed = sharedPrefs.edit();
        ed.putInt(PERIOD_UPDATE, periodUpdate);
        ed.commit();
    }

    public static int getPeriodUpdate() {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        return sharedPrefs.getInt(PERIOD_UPDATE, 5);
    }

    public static void setDisplayDuration(int displayDuration) {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        SharedPreferences.Editor ed = sharedPrefs.edit();
        ed.putInt(DISPLAY_DURATION, displayDuration);
        ed.commit();
    }

    public static int getDisplayDuration() {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        return sharedPrefs.getInt(DISPLAY_DURATION, 3);
    }

    public static void setRandomDisplay(boolean randomDisplay) {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        SharedPreferences.Editor ed = sharedPrefs.edit();
        ed.putBoolean(RANDOM_DISPLAY, randomDisplay);
        ed.commit();
    }

    public static boolean isRandomDisplay() {
        // Save the duration into settings
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(HappyMomentsApp.getApplication());
        return sharedPrefs.getBoolean(RANDOM_DISPLAY, false);
    }
}
