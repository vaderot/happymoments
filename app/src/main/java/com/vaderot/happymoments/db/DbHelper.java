package com.vaderot.happymoments.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Admin on 12.08.2015.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final String INT = "INTEGER";
    public static final String TEXT = "TEXT";
    public static final int DB_VERSION = 1;


    private static SQLiteDatabase sCurrentDb;
    private static Context sContext;

    public DbHelper(Context context) {
        super(context, context.getPackageName(), null, DB_VERSION);

        initDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    public synchronized void initDatabase() {
        sCurrentDb = getWritableDatabase();
    }

    public void createTables(SQLiteDatabase db) {

        // sqlite is updated in android 5.0. DB is working wrong http://stackoverflow.com/questions/27373344/sqlite-database-gives-warning-automatic-index-on-table-namecolumn-after-upgr
        db.rawQuery("PRAGMA automatic_index=off;", null);//for android 5.0

        createTable(db, ImagesTable.getCreation());

    }

    public void createTable(SQLiteDatabase db, String query) {
        db.execSQL(query);
    }

    public void dropTables(SQLiteDatabase db) {
        dropTable(db, ImagesTable.NAME);
    }

    public void dropTable(SQLiteDatabase db, String table) {
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

    public static long insert(String table, String nullColumnHack, ContentValues values) {
        return sCurrentDb.insert(table, nullColumnHack, values);
    }
    public static long insertWithOnConflict(String table, String nullColumnHack, ContentValues values, int conflictAlgorithm) {
        return sCurrentDb.insertWithOnConflict(table, nullColumnHack, values, conflictAlgorithm);
    }

    public static long update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return sCurrentDb.update(table, values, whereClause, whereArgs);
    }
    public static int delete(String table, String whereClause, String[] whereArgs) {
        return sCurrentDb.delete(table, whereClause, whereArgs);
    }

    public static Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return sCurrentDb.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }
    public static Cursor query(String sql) {
        return sCurrentDb.rawQuery(sql, null);
    }


}
