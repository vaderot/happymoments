package com.vaderot.happymoments.maodels;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.vaderot.happymoments.db.ImagesTable;

import java.io.Serializable;

/**
 * Created by Admin on 12.08.2015.
 */
public class Image implements Parcelable {

    private int id;
    private Uri imageUri;
    private String imageName;
    private String imageDeskription;

    public Image() {
    }

    public Image(Parcel in) {
        readFromParcel(in);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
    public void setImageUri(String imageUri) {
        this.imageUri = Uri.parse(imageUri);
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageDeskription() {
        return imageDeskription;
    }

    public void setImageDeskription(String imageDeskription) {
        this.imageDeskription = imageDeskription;
    }

    public static ContentValues toContentValues(Image image){
        ContentValues values = new ContentValues();
        if (image.getImageUri() != null)
            values.put(ImagesTable.Columns.IMAGE_URI.getName(), image.getImageUri().toString());

        if(image.getImageName() != null)
            values.put(ImagesTable.Columns.IMAGE_NAME.getName(), image.getImageName());

        if(image.getImageDeskription() != null)
            values.put(ImagesTable.Columns.IMAGE_DESCRIPTION.getName(), image.getImageDeskription());

        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(imageUri.toString());
        dest.writeString(imageName);
        dest.writeString(imageDeskription);
    }

    private void readFromParcel(Parcel in) {
        id = in.readInt();
        imageUri = Uri.parse(in.readString());
        imageName = in.readString();
        imageDeskription = in.readString();

    }

    public static final Parcelable.Creator<Image> CREATOR = new Creator<Image>() {

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }

        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }
    };
}
