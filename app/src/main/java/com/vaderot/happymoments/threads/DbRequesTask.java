package com.vaderot.happymoments.threads;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vaderot.happymoments.db.DbHelper;
import com.vaderot.happymoments.db.ImagesTable;
import com.vaderot.happymoments.maodels.Image;

/**
 * Created by Admin on 12.08.2015.
 */
public class DbRequesTask extends AsyncTask<Object, Void, Cursor> {

    private RequestType mRequestType;

    public enum RequestType {
        GET_ALL_IMAGES,
        GET_IMAGE,
        INSET_IMAGE,
        DELETE_IMAGE,
        UPDATE_IMAGE;
    }

    public DbRequesTask(RequestType requestType) {
        this.mRequestType = requestType;
    }

    @Override
    protected Cursor doInBackground(Object... params) {
        switch (mRequestType){
            case GET_ALL_IMAGES: {
                String query = "SELECT * FROM " + ImagesTable.NAME;
                return DbHelper.query(query);
            }
            case GET_IMAGE: {
                String query = "SELECT * FROM " + ImagesTable.NAME + " WHERE " + ImagesTable.Columns.ID + " = " + ((Integer) params[0]);
                return DbHelper.query(query);
            }
            case INSET_IMAGE: {
                ContentValues contentValues = Image.toContentValues((Image)params[0]);
                DbHelper.insertWithOnConflict(ImagesTable.NAME, null,contentValues
                        , SQLiteDatabase.CONFLICT_IGNORE);
            }
            break;
            case DELETE_IMAGE:{
                String where = ImagesTable.Columns.ID + " = " + ((Integer)params[0]);
                DbHelper.delete(ImagesTable.NAME, where, null);
            }
            break;
            case UPDATE_IMAGE: {
                Image image = (Image) params[0];
                ContentValues contentValues = Image.toContentValues(image);
                String where = ImagesTable.Columns.ID + " = " + (image.getId());
                DbHelper.update(ImagesTable.NAME, contentValues, where, null);
            }
            break;
        }
        return null;
    }
}