package com.vaderot.happymoments.threads;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;

import com.vaderot.happymoments.Consts;
import com.vaderot.happymoments.activities.ShowImageActivity;
import com.vaderot.happymoments.db.ImagesTable;
import com.vaderot.happymoments.db.PrefHelper;
import com.vaderot.happymoments.maodels.Image;

/**
 * Created by Admin on 12.08.2015.
 */
public class ShowImageTask extends AsyncTask<Void, Void, Void> {

    private Service mContext;
    private Cursor mCursor;

    private int cursorPosition = 0;
    private boolean stopped;

    private boolean showRandom;
    private int displayDuration;
    private int periodUpdate;


    public ShowImageTask(Service context, Cursor cursor) {
        this.mContext = context;
        this.mCursor = cursor;
        stopped = false;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        periodUpdate = PrefHelper.getPeriodUpdate() * 60 * 1000;
        displayDuration = PrefHelper.getDisplayDuration() * 1000 / 10;
        showRandom = PrefHelper.isRandomDisplay();



        while (!stopped){
            publishProgress();

            try {
                Thread.sleep(periodUpdate);
            }catch (InterruptedException ex){
                ex.printStackTrace();
            }
        }

        return null ;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

        if(mCursor != null){
            if(showRandom){
                cursorPosition = (int) (Math.random() *  mCursor.getCount());
                mCursor.moveToPosition(cursorPosition);
            } else {
                if (!mCursor.moveToPosition(cursorPosition)) {
                    cursorPosition = 0;
                    mCursor.moveToFirst();
                }
                ++cursorPosition;
            }
            Image image = ImagesTable.cursorToObject(mCursor);

            Intent intent = new Intent(mContext, ShowImageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Consts.IMAGE_URI, image.getImageUri().toString());
            intent.putExtra(Consts.DISPLAY_DURATION, displayDuration);
            mContext.startActivity(intent);
        }


    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

}
