package com.vaderot.happymoments.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.vaderot.happymoments.R;
import com.vaderot.happymoments.widgets.LoaderImageView;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Admin on 13.08.2015.
 */
public class BaseImageFragment extends Fragment implements View.OnClickListener {
    private static final int REQUEST = 1;

    protected LoaderImageView mImageView;
    protected EditText mImageName;
    protected EditText mImageDescription;

    protected Uri mImageUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_image, container, false);

        mImageView = (LoaderImageView) view.findViewById(R.id.image);
        mImageView.setOnClickListener(this);
        mImageName = (EditText) view.findViewById(R.id.imageName);
        mImageDescription = (EditText) view.findViewById(R.id.imageDescription);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image:
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap = null;

        if (requestCode == REQUEST && resultCode == Activity.RESULT_OK) {
            mImageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mImageView.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}