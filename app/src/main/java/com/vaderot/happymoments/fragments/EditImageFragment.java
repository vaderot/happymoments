package com.vaderot.happymoments.fragments;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.vaderot.happymoments.Consts;
import com.vaderot.happymoments.R;
import com.vaderot.happymoments.activities.BaseFragmentActivity;
import com.vaderot.happymoments.db.ImagesTable;
import com.vaderot.happymoments.maodels.Image;
import com.vaderot.happymoments.threads.DbRequesTask;

/**
 * Created by Admin on 13.08.2015.
 */
public class EditImageFragment extends BaseImageFragment {

    private Image mImage;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarEditImage).setVisibility(View.VISIBLE);

//        initVew();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarEditImage).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
//
//        mProgressBar = (ProgressBar) view.findViewById(R.id.pbLoading);
//        mProgressBar.setVisibility(View.VISIBLE);

        initVew();

        return view;
    }

    private void initVew(){
        int imageId = getArguments().getInt(Consts.IMAGE_ID);
        new DbRequesTask(DbRequesTask.RequestType.GET_IMAGE){
            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                if(cursor != null && cursor.moveToFirst()) {
                    mImage = ImagesTable.cursorToObject(cursor);
                    mImageUri = mImage.getImageUri();
                    mImageName.setText(mImage.getImageName());
                    mImageDescription.setText(mImage.getImageDeskription());

                    mImageView.load(mImageUri.toString());

//                    new AsyncTask<Void, Void, Bitmap>(){
//                        @Override
//                        protected Bitmap doInBackground(Void... voids) {
//                            Bitmap bitmap = null;
//                            try {
//                                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
//                            } catch (FileNotFoundException e) {
//                                e.printStackTrace();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            return bitmap;
//                        }
//
//                        @Override
//                        protected void onPostExecute(Bitmap bitmap) {
//                            super.onPostExecute(bitmap);
//                            mImageView.setImageBitmap(bitmap);
//                            mProgressBar.setVisibility(View.GONE);
//                        }
//                    }.execute();
                }
            }
        }.execute(imageId);

    }



    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public void updateImage(){
        mImage.setImageUri(mImageUri);
        mImage.setImageName(mImageName.getText().toString());
        mImage.setImageDeskription(mImageDescription.getText().toString());

        new DbRequesTask(DbRequesTask.RequestType.UPDATE_IMAGE).execute(mImage);
        getActivity().onBackPressed();
    }

    public void deleteImage(){
        new DbRequesTask(DbRequesTask.RequestType.DELETE_IMAGE).execute(mImage.getId());
        getActivity().onBackPressed();
    }

}
