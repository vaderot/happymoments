package com.vaderot.happymoments.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.NumberPicker;

import com.vaderot.happymoments.R;
import com.vaderot.happymoments.activities.BaseFragmentActivity;
import com.vaderot.happymoments.db.PrefHelper;

/**
 * Created by Admin on 14.08.2015.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener {

    private NumberPicker mPeriodUpdate;
    private NumberPicker mDisplayDuration;
    private CheckBox mRandomDisplay;
    private Button mSaveSettings;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarSettings).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarSettings).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        mPeriodUpdate = (NumberPicker) view.findViewById(R.id.periodUpdate);
        mDisplayDuration = (NumberPicker) view.findViewById(R.id.displayDuration);
        mRandomDisplay = (CheckBox) view.findViewById(R.id.randomDisplay);
        mSaveSettings = (Button) view.findViewById(R.id.btSaveSettings);
        mSaveSettings.setOnClickListener(this);

        initView();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSaveSettings:{
                PrefHelper.setPeriodUpdate(mPeriodUpdate.getValue());
                PrefHelper.setDisplayDuration(mDisplayDuration.getValue());
                PrefHelper.setRandomDisplay(mRandomDisplay.isChecked());

                getActivity().onBackPressed();
            }
            break;
        }

    }

    private void initView(){
        mPeriodUpdate.setMinValue(1);
        mPeriodUpdate.setMaxValue(100);
        mPeriodUpdate.setValue(PrefHelper.getPeriodUpdate());

        String[] displayedValues = {"0,1", "0,2", "0,3", "0,4", "0,5", "0,6", "0,7", "0,8", "0,9", "1"};
        mDisplayDuration.setMinValue(1);
        mDisplayDuration.setMaxValue(10);
        mDisplayDuration.setDisplayedValues(displayedValues);
        mDisplayDuration.setValue(PrefHelper.getDisplayDuration());

        mRandomDisplay.setChecked(PrefHelper.isRandomDisplay());

    }

}
