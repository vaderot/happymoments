package com.vaderot.happymoments.fragments;

import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vaderot.happymoments.R;
import com.vaderot.happymoments.adapters.ImageRecyclerAdapter;
import com.vaderot.happymoments.threads.DbRequesTask;
import com.vaderot.happymoments.widgets.FloatingActionButton;

/**
 * Created by Admin on 12.08.2015.
 */
public class MainFragment extends Fragment{

    private RecyclerView mImagesRecycler;
    private ImageRecyclerAdapter mImageRecyclerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageRecyclerAdapter = new ImageRecyclerAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mImagesRecycler = (RecyclerView) view.findViewById(R.id.imagesList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mImagesRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getActivity());
        mImagesRecycler.setLayoutManager(layoutManager);
        mImagesRecycler.setAdapter(mImageRecyclerAdapter);
        new DbRequesTask(DbRequesTask.RequestType.GET_ALL_IMAGES){
            @Override
            protected void onPostExecute(Cursor cursor) {
                mImageRecyclerAdapter.changeCursor(cursor);
                mImageRecyclerAdapter.notifyDataSetChanged();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton fabAddImage = new FloatingActionButton.Builder(MainFragment.this)
                .withDrawable(getActivity().getResources().getDrawable(R.drawable.ic_plus_white))
                .withButtonColor(Color.BLUE)
                .withGravity(Gravity.BOTTOM | Gravity.RIGHT)
                .withMargins(0, 0, 16, 16)
                .create();
        fabAddImage.setId(R.id.fabAddImage);
        fabAddImage.setOnClickListener( (View.OnClickListener) getActivity());
    }
}