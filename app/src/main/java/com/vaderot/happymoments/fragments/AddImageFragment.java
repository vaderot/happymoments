package com.vaderot.happymoments.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.vaderot.happymoments.R;
import com.vaderot.happymoments.activities.BaseFragmentActivity;
import com.vaderot.happymoments.threads.DbRequesTask;
import com.vaderot.happymoments.maodels.Image;

public class AddImageFragment extends BaseImageFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarAddImage).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseFragmentActivity)getActivity()).getToolbar().findViewById(R.id.toolbarAddImage).setVisibility(View.GONE);
    }

    public void addImage(){
        if(mImageUri == null){
            Toast.makeText(getActivity(), getString(R.string.image_choose_toast), Toast.LENGTH_SHORT).show();
            return;
        }
        Image image = new Image();
        image.setImageUri(mImageUri);
        image.setImageName(mImageName.getText().toString());
        image.setImageDeskription(mImageDescription.getText().toString());

        new DbRequesTask(DbRequesTask.RequestType.INSET_IMAGE).execute(image);

        getActivity().onBackPressed();
    }
}
